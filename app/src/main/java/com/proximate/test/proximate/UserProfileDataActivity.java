package com.proximate.test.proximate;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.proximate.test.proximate.database.Database;
import com.proximate.test.proximate.webApi.PRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserProfileDataActivity extends AppCompatActivity {

    private String session = null;
    public Boolean userCacheActivated = false;
    public Database db = null;
    public final int CAMERA_INTENT = 0;
    public TextView user;
    public TextView mail;
    public TextView last;
    private View mProgressView;
    private Button cameraButton;
    private ImageView profilePhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_data);
        Bundle loginData = getIntent().getExtras();
        user = (TextView) findViewById(R.id.user_name);
        mail = (TextView) findViewById(R.id.mail);
        last = (TextView) findViewById(R.id.last_session);
        profilePhoto = (ImageView) findViewById(R.id.profile_photo);
        mProgressView = (View) findViewById(R.id.progress_bar);
        cameraButton = (Button) findViewById(R.id.camera);


        if(loginData != null && loginData.getString("token_auth") != null) {
            session = loginData.getString("token_auth");
        } else if(loginData != null && loginData.getBoolean("user_cache")){
            userCacheActivated = true;
        }

        profilePhoto.setBackground(Drawable.createFromPath(getImagePath()));


        db = Database.instance(this);
        db.openDB();


        if(userCacheActivated) {
            db.getUserData(null,null);
            initUI();
            showProgress(false);
        } else {
            getUserData(session);
        }

        /*try {
            InputStream is = new ByteArrayInputStream(getImagePath().getBytes());
            ExifInterface photoDetails = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                photoDetails = new ExifInterface(is);
            }
            String lat = ExifInterface.TAG_GPS_LATITUDE;
            String lat_data = photoDetails.getAttribute(lat);
            String lng = ExifInterface.TAG_GPS_LONGITUDE;
            String lng_data = photoDetails.getAttribute(lng);
            Log.d("Photo Info",lat_data + ", " + lng_data);
        } catch (IOException e) {
            e.printStackTrace();
        }*/


    }

    public void getUserData(String token_auth) {

        String profileUrl = PRequest.instance().HOST + PRequest.instance().PROFILE_URL;
        PRequest.instance().EXTRA_HEADER = "Authorization";
        PRequest.instance().EXTRA_VALUE = token_auth;

        showProgress(true);

        PRequest.instance().POST(profileUrl,new JSONObject().toString(), new PRequest.response() {
            @Override
            public void success(String json, InputStream input) {
                Log.d("User_response",json);
                String msg = "";
                try {
                    JSONObject res = new JSONObject(json);
                    JSONArray userResponse = res.getJSONArray("data");
                    JSONObject data = userResponse.getJSONObject(0);

                    PUserProfile.instance().server_id = data.getInt("id");
                    PUserProfile.instance().nombres = data.getString("nombres");
                    PUserProfile.instance().apellidos = data.getString("apellidos");
                    PUserProfile.instance().correo = data.getString("correo");
                    PUserProfile.instance().numero_documento = data.getDouble("numero_documento");
                    PUserProfile.instance().ultima_sesion = data.getString("ultima_sesion");
                    PUserProfile.instance().estados_usuarios_label = data.getString("estados_usuarios_label");

                    db.createRecords();
                    showProgress(false);

                    initUI();

                    msg = res.optString("message");
                    Log.d("Proximate UserProfile","Datos de usuario guardados correctamente");

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("Proximate UserProfile","Error al guardar datos de usuario");

                }
               // launchDialog(PRequest.instance().HOST,msg,null,null,null);
            }

            @Override
            public void fail(String json, InputStream input) {
                Log.e("User_response",json);
                String msg = "";
                try {
                    JSONObject res = new JSONObject(json);
                    msg = res.optString("message");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                showProgress(false);
                launchDialog(PRequest.instance().HOST,msg,null,null,null);
            }
        });

    }

    public void launchDialog(String title, String msg, final Boolean error, final Boolean success,final String token) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
            }
        }).setCancelable(false);
        AlertDialog launch = builder.show();
        launch.show();
    }

    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(requestCode == CAMERA_INTENT && resultCode == RESULT_OK) {
            Log.i("photomaker", "Imagen guardada correctamente");

            profilePhoto.setBackgroundDrawable(Drawable.createFromPath(getImagePath()));
            profilePhoto.invalidate();
        }

    }

    public void initUI() {
        user.setText(getString(R.string.name) + " " + PUserProfile.instance().nombres + " " + PUserProfile.instance().apellidos);
        mail.setText(getString(R.string.mail) + " " + PUserProfile.instance().correo);

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date_last_session = null;
        try {
            date_last_session = (Date)formatter.parse(PUserProfile.instance().ultima_sesion);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MMMM/yyyy");
        String mDate = dateFormat.format(date_last_session);

        SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
        String mHour = hourFormat.format(date_last_session);

        last.setText(getString(R.string.last) + " " + mDate + " " + getString(R.string.last_hour) + " " + mHour);

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String photo = getDir() + "profile.jpg";
                File newPhoto = new File(photo);


                try {
                    newPhoto.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Uri photoURI = FileProvider.getUriForFile(UserProfileDataActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        newPhoto);
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                i.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(i, CAMERA_INTENT);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_logout) {
            logOut();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
    }

    public String getImagePath() {
        String path = Environment.getExternalStorageDirectory() + "/" + "proximate/" + "profile.jpg";
        return path;
    }

    public void logOut() {
        if(db.deleteDatabase()) {
            db.closeDB();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public String getDir(){

        String path = Environment.getExternalStorageDirectory() + "/" + "proximate/";
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        return path;
    }


}

package com.proximate.test.proximate.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.proximate.test.proximate.PUserProfile;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by erodriguez on 09/09/17.
 */

public class Database {

    private static Database instance = null;
    private static Context mContext;

    private static SQLiteDatabase database;

    public static Database instance(Context c) {

        if (instance == null) {
            instance = new Database(c);
        }
        return instance;
    }


    public final static String USERS_TABLE="proximate_users"; // tabla
    public final static String USER_ID="_id"; // id de registro de usuario bd local
    public final static String USER_ID_SERVER="proximate_id"; // id de usuario en server
    public final static String USER_NAME="name";  // nombre completo
    public final static String USER_LAST_NAME="last_name";  // nombre completo
    public final static String USER_MAIL="email"; // correo electrónico
    public final static String USER_IMAGE="image"; // path imagen de perfil
    public final static String USER_LAST_SESSION="last_session"; // fecha de última sesión
    public final static String USER_DOCUMENTS_ID="documents_id"; // id de documentos asignados
    public final static String USER_STATUS="status"; // status de usuario bandera eliminado 0 activo,
    public  static String DATABASE_PATH;

    /**
     *
     * @param context
     */
    public Database(Context context){
        database = DatabaseSQLiteHelper.instance(context).getWritableDatabase();
        mContext = context;
        DATABASE_PATH = "/data/data/" + mContext.getPackageName() + "/databases/proximate_DB";
    }


    public long createRecords(){
        ContentValues values = new ContentValues();
        values.put(USER_ID_SERVER, PUserProfile.instance().server_id);
        values.put(USER_NAME,PUserProfile.instance().nombres);
        values.put(USER_LAST_NAME,PUserProfile.instance().apellidos);
        values.put(USER_MAIL, PUserProfile.instance().correo);
        values.put(USER_IMAGE, PUserProfile.instance().image_path);
        values.put(USER_LAST_SESSION, PUserProfile.instance().ultima_sesion);
        values.put(USER_DOCUMENTS_ID, PUserProfile.instance().numero_documento);
        values.put(USER_STATUS, PUserProfile.instance().estados_usuarios_label);

        return database.insert(USERS_TABLE, null, values);
    }

    public  int updateSession(int value,String[] id) {

        ContentValues cv = new ContentValues();
        cv.put(USER_LAST_SESSION,value);

        return database.update(USERS_TABLE, cv, USER_ID + " = ?", id);
    }

    public void getUserData(String query,String[] args) {
        String[] cols = new String[] {USER_ID,USER_ID_SERVER, USER_NAME,USER_LAST_NAME,USER_MAIL,USER_IMAGE,
                USER_LAST_SESSION,USER_DOCUMENTS_ID,USER_STATUS};

        Cursor mCursor ;

        if(query!=null&&args!=null){
            mCursor = database.query(true,USERS_TABLE, cols, query, args, null,null, USER_ID + " DESC", null);
        }
        else{
            mCursor = database.query(true, USERS_TABLE, cols, null, null, null, null, USER_ID + " DESC", null);
        }

        if (mCursor != null)
            mCursor.moveToFirst();

        int column_id = mCursor.getColumnIndex(USER_ID);
        int column_id_server = mCursor.getColumnIndex(USER_ID_SERVER);
        int column_user_name = mCursor.getColumnIndex(USER_NAME);
        int column_user_last_name = mCursor.getColumnIndex(USER_LAST_NAME);
        int column_mail = mCursor.getColumnIndex(USER_MAIL);
        int column_image = mCursor.getColumnIndex(USER_IMAGE);
        int column_last_session = mCursor.getColumnIndex(USER_LAST_SESSION);
        int column_document_id = mCursor.getColumnIndex(USER_DOCUMENTS_ID);
        int column_status= mCursor.getColumnIndex(USER_STATUS);


        for(mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {

                PUserProfile.instance().id = mCursor.getInt(column_id);
                PUserProfile.instance().server_id = mCursor.getInt(column_id_server);
                PUserProfile.instance().nombres = mCursor.getString(column_user_name);
                PUserProfile.instance().apellidos = mCursor.getString(column_user_last_name);
                PUserProfile.instance().correo = mCursor.getString(column_mail);
                PUserProfile.instance().image_path = mCursor.getString(column_image);
                PUserProfile.instance().ultima_sesion = mCursor.getString(column_last_session);
                PUserProfile.instance().numero_documento = mCursor.getDouble(column_document_id);
                PUserProfile.instance().estados_usuarios_label = mCursor.getString(column_status);
        }
    }

    public void closeDB() {
        database.close();
    }

    public void openDB() {
        database = DatabaseSQLiteHelper.instance(mContext).getWritableDatabase();
    }

    public boolean deleteDatabase() {
        return mContext.deleteDatabase(DATABASE_PATH);
    }
}

package com.proximate.test.proximate;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.proximate.test.proximate.database.Database;
import com.proximate.test.proximate.webApi.PRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {

    private String USER_NAME = "correo";  //prueba@proximateapps.com
    private String PASS = "contrasenia"; //12digo16digo18#$
    private View mProgressView;
    public Database db = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button loginBtn = (Button) findViewById(R.id.login_button);
        final EditText user_txt = (EditText) findViewById(R.id.user_name);
        final EditText pass_txt = (EditText) findViewById(R.id.pass);
        mProgressView = (View) findViewById(R.id.login_progress);
        requestPermmisions();

        db = Database.instance(this);

        db.openDB();

        if(isUserStored()) {
            startActivity(new Intent(MainActivity.this,UserProfileDataActivity.class).putExtra("user_cache",true));
        }


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(MainActivity.this,"Login action",Toast.LENGTH_SHORT).show();
                try {
                     if(isUserStored()) {
                         db.deleteDatabase();
                     }
                     login(user_txt.getText().toString(),pass_txt.getText().toString());
                    //login("prueba@proximateapps.com","12digo16digo18#$");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finish();
                }
                return;


        }
    }


    public void login(String user, String pass) throws UnsupportedEncodingException {

        String loginUrl = PRequest.instance().HOST + PRequest.instance().LOGIN_URL;

        JSONObject requestData = new JSONObject();

        try {
            requestData.put(USER_NAME,user);
            requestData.put(PASS,pass);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        showProgress(true);

        PRequest.instance().POST(loginUrl, requestData.toString(), new PRequest.response() {
            @Override
            public void success(String json, InputStream input) {
                JSONObject response;
                String message = "";
                String sessionToken = "";
                Boolean success = false;
                Boolean error = false;
                try {
                    response = new JSONObject(json);
                    message =  response.get("message").toString();
                    error = response.getBoolean("error");
                    success = response.getBoolean("success");
                    sessionToken = response.optString("token");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("Login repsonse", json);
                showProgress(false);
                launchDialog(PRequest.instance().HOST,message,error,success,sessionToken);
            }

            @Override
            public void fail(String json, InputStream input) {
                Log.e("Login error", json);
                showProgress(false);
                launchDialog(PRequest.instance().HOST,"Error de comunicaciones",null,null,null);

            }
        });

    }

    public void launchDialog(String title, String msg, final Boolean error, final Boolean success,final String token) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!error && success){
                    db.closeDB();
                    startActivity(new Intent(MainActivity.this,UserProfileDataActivity.class).putExtra("token_auth",token));
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                }
            }
        }).setCancelable(false);
        AlertDialog launch = builder.show();
        launch.show();
    }


    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private boolean isUserStored() {
        db.getUserData(null,null);
        return PUserProfile.instance().id != null;
    }



    public void requestPermmisions(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this,
                    Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        1);
            }
        }

    }

}

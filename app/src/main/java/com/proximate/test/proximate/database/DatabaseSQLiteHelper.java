package com.proximate.test.proximate.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by erodriguez on 09/09/17.
 */

public class DatabaseSQLiteHelper extends SQLiteOpenHelper{

    private static DatabaseSQLiteHelper instance;

    // ...

    public static synchronized DatabaseSQLiteHelper instance(Context context) {

        if (instance == null) {
            instance = new DatabaseSQLiteHelper(context.getApplicationContext());
        }
        return instance;
    }



    public static final String DATABASE_NAME = "proximate_DB";

    private static final int DATABASE_VERSION = 2;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "CREATE TABLE proximate_users "
            + " ( _id INTEGER PRIMARY KEY," // ID de la base de datos local
            + " proximate_id INTEGER," // ID del server
            + " name TEXT NOT NULL,"
            + " last_name TEXT,"
            + " email TEXT,"
            + " image TEXT,"
            + " last_session TEXT,"
            + " documents_id INTEGER,"
            + " status TEXT"  // Status usuario
            + " ); ";

    public DatabaseSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    // Method is called during an upgrade of the database,
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion){
        database.execSQL("DROP TABLE IF EXISTS proximate_users");
        onCreate(database);
    }
}

package com.proximate.test.proximate.webApi;

import android.text.format.Formatter;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.ByteArrayEntity;

/**
 * Created by erodriguez on 08/09/17.
 */

public class PRequest {

    public String CONTENT_TYPE = "application/json";
    public String AUTH_TYPE = "Basic";
    final int DEFAULT_TIMEOUT = 120 * 1000;
    private static final int CONNECTION_TIMEOUT = 1000;
    public boolean hasServerResponse = false;


    private static PRequest singleTone = null;
    public  String HOST = "https://serveless.proximateapps-services.com.mx";

    public  String LOGIN_URL = "/catalog/dev/webadmin/authentication/login";
    public  String PROFILE_URL = "/catalog/dev/webadmin/users/getdatausersession";

    public String EXTRA_HEADER = "";
    public String EXTRA_VALUE = "";



    public interface response {

        public void success(String json, InputStream input);
        public void fail(String json,InputStream input);

    }


    public static PRequest instance() {
        if(singleTone == null) {
            singleTone = new PRequest();
        }

        return singleTone;
    }

    public void GET(String urlStr, final response mci) {

        String fullUrl = urlStr;
        Log.i("try GET ", fullUrl);
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setResponseTimeout(DEFAULT_TIMEOUT);
        client.setMaxRetriesAndTimeout(2, CONNECTION_TIMEOUT);
        PRequest.instance().setHttpAdditionalHeaders(client);
        client.get(null, fullUrl, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i("","onSuccess");
                Log.i("statusCode:" , String.valueOf(statusCode));

                if(responseBody == null) {
                    Log.i("","onFailure No Server Response");
                    hasServerResponse = false;
                    mci.fail(null, null);
                    return;
                }

                hasServerResponse = true;
                String json =  new String(responseBody);
                Log.i("onSuccess:" , json.toString());
                InputStream is = new ByteArrayInputStream(responseBody);
                mci.success(json, is);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i("FAILURE","onFailure");
                Log.i("statusCode:", String.valueOf(statusCode));
                if(responseBody == null) {
                    Log.i("","onFailure No Server Response");
                    hasServerResponse = false;
                    mci.fail(null, null);
                    return;
                }
                hasServerResponse = true;
                String json =  new String(responseBody);
                Log.i("onFailure:", json.toString());
                InputStream is = new ByteArrayInputStream(responseBody);
                try {


                    mci.fail(json, is);

                } catch (Exception ignored) {

                    ignored.printStackTrace();
                    Log.e("UserKey " ,ignored.getMessage());
                    mci.fail(json, is);

                }
            }

        });
    }


    /////////////////////////////////////////////


    public void POST(String urlStr, String bodyData, final response mci) {

        String fullUrl = urlStr;
        Log.i("try POST ", fullUrl);


        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setResponseTimeout(DEFAULT_TIMEOUT);
        client.setMaxRetriesAndTimeout(2, CONNECTION_TIMEOUT);
        PRequest.instance().setHttpAdditionalHeaders(client);
        Log.i("bodyData:" , bodyData);
        String j = bodyData;

        try {
            ByteArrayEntity entity = new ByteArrayEntity(j.getBytes("UTF-8"));

            client.post(null,fullUrl, entity, null, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Log.i("","onSuccess");
                    Log.i("statusCode:", String.valueOf(statusCode));

                    if(responseBody == null) {
                        Log.e("","onFailure No Server Response");
                        hasServerResponse = false;
                        mci.fail(null, null);
                        return;
                    }
                    hasServerResponse = true;
                    String json = new String(responseBody);
                    Log.i("onSuccess:" , json.toString());
                    InputStream is = new ByteArrayInputStream(responseBody);
                    mci.success(json, is);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.i("","onFailure");
                    Log.i("statusCode:" ,String.valueOf(statusCode));
                    if(responseBody == null) {
                        hasServerResponse = false;
                        Log.e("","onFailure No Server Response");
                        mci.fail(null, null);
                        return;
                    }
                    hasServerResponse = true;
                    String json =  new String(responseBody);
                    Log.i("onFailure:", json.toString());
                    InputStream is = new ByteArrayInputStream(responseBody);

                    try {

                        mci.fail(json, is);

                    } catch (Exception ignored) {

                        ignored.printStackTrace();
                        Log.e("UserKey " ,ignored.getMessage());
                        mci.fail(json, is);

                    }
                }


            });

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }


    /////////////////////////////////////////////


    class PHeader{

        public String name;
        public String value;

        public PHeader(String n, String v) {
            this.name = n;
            this.value = v;
        }

        @Override
        public String toString() {
            return "\n " + name + " : " + value + " ";
        }

    }

    void setHttpAdditionalHeaders(AsyncHttpClient client){

        List<PHeader> headers = new ArrayList<>();

        headers.add(new PHeader("Content-Type", CONTENT_TYPE));
        headers.add(new PHeader("Accept", CONTENT_TYPE));

        if(!EXTRA_HEADER.isEmpty() && !EXTRA_VALUE.isEmpty())
         headers.add(new PHeader(EXTRA_HEADER, EXTRA_VALUE));



        for(PHeader h : headers){
            client.addHeader(h.name, h.value);
        }

        Log.i("headers: ", headers.toString());

    }
}

package com.proximate.test.proximate;

import java.util.Date;

/**
 * Created by erodriguez on 09/09/17.
 */

public class PUserProfile {

    public Integer id = null;
    public Integer server_id = null;
    public String nombres = "";
    public String apellidos = "";
    public String correo = "";
    public Double numero_documento = null;
    public String image_path = "";
    public String ultima_sesion = "";
    public String estados_usuarios_label = "";
    private static PUserProfile singleTone = null;



    public static PUserProfile instance() {
        if(singleTone == null) {
            singleTone = new PUserProfile();
        }

        return singleTone;
    }




}
